﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AspCrudEF.Models
{
    public class Veiculo
    {
        public int Id { get; set; }
        public string Marca { get; set; }
        public string Descricao { get; set; }
        public string Ano { get; set; }
    }
}