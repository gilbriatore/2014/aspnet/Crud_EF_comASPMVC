﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AspCrudEF.Models;

namespace AspCrudEF.Controllers
{
    public class HomeController : Controller
    {
        Contexto ctx = new Contexto();

        //
        // GET: /Home/

        public ActionResult Index()
        {            
            List<Veiculo> lista = ctx.Veiculos.ToList();
            return View(lista);
        }

        //
        // GET: /Home/Details/5

        public ActionResult Details(int id)
        {
            Veiculo veiculo = ctx.Veiculos.Find(id);
            return View(veiculo);
        }

        //
        // GET: /Home/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Home/Create

        [HttpPost]
        public ActionResult Create(Veiculo veiculo)
        {
            try
            {
                ctx.Veiculos.Add(veiculo);
                ctx.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Home/Edit/5

        public ActionResult Edit(int id)
        {
            Veiculo veiculo = ctx.Veiculos.Find(id);
            return View(veiculo);
        }

        //
        // POST: /Home/Edit/5

        [HttpPost]
        public ActionResult Edit(Veiculo veiculo)
        {
            try
            {
                ctx.Entry(veiculo).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Home/Delete/5

        public ActionResult Delete(int id)
        {
            Veiculo veiculo = ctx.Veiculos.Find(id);
            return View(veiculo);
        }

        //
        // POST: /Home/Delete/5

        [HttpPost]
        public ActionResult Delete(Veiculo veiculo)
        {
            try
            {
                ctx.Entry(veiculo).State = System.Data.Entity.EntityState.Deleted;
                ctx.Veiculos.Remove(veiculo);
                ctx.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
